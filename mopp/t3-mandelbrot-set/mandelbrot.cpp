#include <complex>
#include <iostream>
#include <stdlib.h>
#include <cctype>
#include <pthread.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <assert.h>
using namespace std;

struct thread_work_t{
	int start_r;
	int end_r;
	char** ptr_mat;
	int *list;
};

void* draw(void*);

int max_row, max_column, max_n, cpus, iteration;

void* draw(void* thread_work_uncasted ){
	int mr=max_row;
	int mc=max_column;
	struct thread_work_t *thread_work = (struct thread_work_t*)thread_work_uncasted ;
	int s_r = thread_work->start_r;
	int e_r = thread_work->end_r;
	int *this_list=(int*)malloc((max_row/cpus)*sizeof(int));
	this_list=thread_work->list;
	thread_work->ptr_mat=(char**)malloc(sizeof(char*)*iteration);
	for (int i=0; i<iteration;i++){
		thread_work->ptr_mat[i]=(char*)malloc(sizeof(char)*mc);
	}
	for(int r = 0; r < iteration; r++){
		
		int l=this_list[r]; 
		
		for(int c = 0; c < mc; c++){
			complex<float> z;
			int n = 0;
			while(abs(z) < 2 && ++n < max_n)
				z = pow(z, 2) + decltype(z)(
					(float)c * 2 / mc - 1.5,
					(float)l * 2 / mr - 1
				);
			thread_work->ptr_mat[r][c]=(n == max_n ? '#' : '.');
		}
	}
}

void* draw_mainThread(void* thread_work_uncasted ){
	int mr=max_row;
	int mc=max_column;
	struct thread_work_t *thread_work = (struct thread_work_t*)thread_work_uncasted ;
	int s_r = thread_work->start_r;
	int e_r = thread_work->end_r;

	thread_work->ptr_mat=(char**)malloc(sizeof(char*)*mr);
	
	for (int i=s_r-1; i<e_r;i++){
		thread_work->ptr_mat[i]=(char*)malloc(sizeof(char)*mc);
	}
	for(int r = s_r-1; r < e_r; r++){
		
		for(int c = 0; c < mc; c++){
			complex<float> z;
			int n = 0;
			while(abs(z) < 2 && ++n < max_n)
				z = pow(z, 2) + decltype(z)(
					(float)c * 2 / mc - 1.5,
					(float)r * 2 / mr - 1
				);
			thread_work->ptr_mat[r][c]=(n == max_n ? '#' : '.');
		}
	}
}

int main(){

	cpus = get_nprocs();
	if (getenv("MAX_CPUS")) {
		cpus = atoi(getenv("MAX_CPUS"));
	}
        assert(cpus > 0 && cpus <= 64);

	cin >> max_row;
	cin >> max_column;
	cin >> max_n;
	char **mat = (char**)malloc(sizeof(char*)*max_row);
	for (int i=0; i<max_row;i++)
		mat[i]=(char*)malloc(sizeof(char)*max_column);

	struct thread_work_t tw[cpus];
	pthread_t thread[cpus];
	iteration=max_row/cpus;
	for (int i=0; i < cpus; i++) {
		tw[i].start_r = (max_row/cpus)* i    + 1;
       		tw[i].end_r   = (max_row/cpus)*(i+1);
		tw[i].list=(int*)malloc((max_row/cpus)*sizeof(int));
		for(int j=0;j<max_row/cpus;j++){
			tw[i].list[j]=(j*cpus)+i;
		}
		       	
        	pthread_create(&thread[i], NULL, draw, (void*)&tw[i]);
	}

	struct thread_work_t maintw;

	maintw.start_r = (max_row/cpus) * cpus + 1;
	maintw.end_r   = max_row;
	
	draw_mainThread((void*)&maintw);

	for (int i=0; i<cpus; i++) {
		// wait for all threads
		pthread_join(thread[i], NULL);		
		
	}
	
	for(int i=0;i<cpus;i++){
		for(int r=0;r<iteration;r++){
			int l=tw[i].list[r];
			strcpy(mat[l],tw[i].ptr_mat[r]);
		}
	}
	//copy mainthread' results
	
	for(int r=maintw.start_r-1;r<maintw.end_r;r++){
		strcpy(mat[r],maintw.ptr_mat[r]);
	}
	// write data out
	for(int r = 0; r < max_row; ++r){
		for(int c = 0; c < max_column; ++c)
			std::cout << mat[r][c];
		cout << '\n';
	}	
}

