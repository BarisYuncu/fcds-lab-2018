#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <climits>
#include <string.h>
#include <pthread.h>
#include <sys/sysinfo.h>
#include <assert.h>
using namespace std;
std::string str;
string sss;
vector<string> arr ;
int lines,cpus;
void* shortest_threaded(void* );
struct thread_work_t{
	int tid;
	int start_r;
	int end_r;
        int max1;
        int a1; 
	int b1;
        string res_str1;
	vector<string> thread_arr ;
};
/*
void write(){
	ofstream outputfile;
	outputfile.open("judge_1.out");
	outputfile<<sss;
	outputfile.close();
}*/
void read(){

	char buf[260];
	lines=std::stoi(fgets(buf,260,stdin)); // first line is number of lines
	while(fgets(buf,260,stdin)!=NULL){
		strtok(buf,"\n");
		arr.push_back(buf);
	}
}

int findOverlap(string s1, string s2, string& str)
{
    int max = INT_MIN;
    int m = s1.length();
    int n = s2.length();
    for (int i = 1; i <= min(m, n); i++){
        if (s1.compare(m - i, i, s2, 0, i) == 0){
            if (max < i){
                max = i;
                str = s1 + s2.substr(i);
            }
        }
    }

    for (int i = 1; i <= min(m, n); i++){
        if (s1.compare(0, i, s2, n - i, i) == 0){
            if (max < i){
                //update
                max = i;
                str = s2 + s1.substr(i);
            }
        }
    }

    return max;
}

void shortest() 
{
    int size = arr.size();
    // run n-1 times, checks every pair
    // can be parallelized, distributed checks by threads.

    while (size != 1)
    {
        int max = INT_MIN;
        int a, b;
        string res_str;
// beginning of threadwork
//puts("spawning threads");
	struct thread_work_t tw[cpus];
	pthread_t thread[cpus];
	for (int i=0; i < cpus; i++) {
		tw[i].tid=i;
		for(int j=0;j<size;j++){
			tw[i].thread_arr.push_back(arr.at(j));
			//cout<<tw[i].thread_arr.at(j)<<endl;
		}
		tw[i].start_r = (size/cpus)* i + 1;
       		tw[i].end_r   = (size/cpus)*(i+1);   
		//printf("Thread %d : from %d to %d \n",i ,tw[i].start_r,tw[i].end_r); 	
        	pthread_create(&thread[i], NULL, shortest_threaded, (void*)&tw[i]);
		//pthread_join(thread[i], NULL); //for testing, delete later
	}
	struct thread_work_t maintw;
	int winner=0;
	if(cpus>1){
		// main thread takes care of the leftover
		maintw.tid=cpus;
		maintw.start_r = (size/cpus) * cpus + 1;
		maintw.end_r   = size;
		for(int j=0;j<size;j++){
			maintw.thread_arr.push_back(arr.at(j));
		}
		shortest_threaded((void*)&maintw);
		sss=maintw.thread_arr.at(0);
		//cout<<"main thread finishes => "<<sss<<endl;
		winner=cpus;
		max=maintw.max1;
	}
	size--;
	for (int i=0; i<cpus; i++) {
		// wait for all threads
		pthread_join(thread[i], NULL);	
		//printf("thread %d ends \n",i);
		if(max<tw[i].max1){
			max=tw[i].max1;
			winner=i;
		}
	
	}//end of collection threads 
	if (max == INT_MIN){
         	arr[0] += arr[size];
	}
       	else{
		if(winner==cpus){
			a=maintw.a1;
			b=maintw.b1;
			arr[a] = maintw.res_str1;
          		arr[b] = maintw.thread_arr[size];
		}else{
			a=tw[winner].a1;
			b=tw[winner].b1;
           		arr[a] = tw[winner].res_str1; // copy resultant string to index a

           		arr[b] = tw[winner].thread_arr[size]; // copy string at last index to index b
		}
       	}
    }
    sss=arr[0];
}
void* shortest_threaded(void* thread_work_uncasted){
    
    struct thread_work_t *thread_work = (struct thread_work_t*)thread_work_uncasted ;
    int s_r = thread_work->start_r;
    int e_r = thread_work->end_r;
    int size=thread_work->thread_arr.size(); // get size of array
    int max = INT_MIN;
    int a=INT_MIN, b=INT_MIN;
    string res_str;
        for (int i = s_r-1; i < e_r; i++){
            for (int j = i + 1; j < size; j++) {
                string str;
                int r = findOverlap(thread_work->thread_arr[i], thread_work->thread_arr[j], str);
                if (max < r){
                    max = r;
                    res_str.assign(str);
                    a = i, b = j;
                }// end of if
            }
        }// end of for
	thread_work->max1=max;
	thread_work->a1=a;
	thread_work->b1=b;
	thread_work->res_str1=res_str;

}// end of shortest_threaded
int main(int argc, char *argv[])
{
    cpus = get_nprocs();
    if (getenv("MAX_CPUS")) {
        cpus = atoi(getenv("MAX_CPUS"));
    }
    assert(cpus > 0 && cpus <= 64);
    //fprintf(stderr, "Running on %d CPUS\n", cpus);
    read();
    shortest();
    cout << sss<<endl;
    //write();
    return 0;
}
