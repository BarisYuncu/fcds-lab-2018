#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <sys/sysinfo.h>

int nNodes;
short int* graph;

struct thread_work_t{
    short int start_n;
    short int end_n;
    short int *ptr;
};

void* warshall(void* );
void read();
void write(FILE *fl);

void write(FILE *fl){
  int i, j;

  for( i=0; i<nNodes; i++ ){
    for( j=0; j<nNodes; j++ )
      fprintf( fl, "%d ", graph[i * nNodes + j] );

    fprintf( fl, "\n");
  }
}

void read (){

  char line[50];
  char* token;
  int size = 50;

  int l;
  int c;

  fgets(line,size,stdin);

  while(!feof(stdin)){

    token = strtok(line," "); // split using space as divider
    if(*token == 'p') {

      token = strtok(NULL," "); // sp

      token = strtok(NULL," "); // no. of vertices
      nNodes = atoi(token);

      token = strtok(NULL," "); // no. of directed edges

      graph = (short int*) malloc(nNodes * nNodes * sizeof (short int));
      if (graph == NULL) {
        printf( "Error in graph allocation: NULL!\n");
        exit( EXIT_FAILURE);
      }

      for(int i = 0; i < nNodes;i++){
        for(int j = 0; j < nNodes;j++){
          graph[i*nNodes+j] = 0;
        }
      }

    } else if(*token == 'a'){
      token = strtok(NULL," ");
      l = atoi(token)-1;

      token = strtok(NULL," ");
      c = atoi(token)-1;

      token = strtok(NULL," ");
      graph[l*nNodes+c] = 1;

    }
    fgets(line,size,stdin);
  }
}




void *warshall(void *thread_work_uncasted ){

    struct thread_work_t *thread_work = (struct thread_work_t*)thread_work_uncasted;
    //const long unsigned int d = thread_work->d;
    const short int s_n = thread_work->start_n;
    const short int e_n = thread_work->end_n;

  for (int k = s_n-1; k < e_n; k++){
    for (int i = 0; i < nNodes; i++){ // row
      for (int j = 0; j < nNodes; j++){ // col
	/* validate i is reachable from j*/
        if(graph[i * nNodes + k] + graph[k * nNodes + j] == 2 && i != j){
          graph[i * nNodes + j] = 1;
	}
      }
    }
  }
}

int main( int argc, char *argv[] ){

    int cpus = get_nprocs();
    if (getenv("MAX_CPUS")) {
        cpus = atoi(getenv("MAX_CPUS"));
    }
    assert(cpus > 0 && cpus <= 64);

    read();

    short int n_graph[cpus][nNodes*nNodes];

    struct thread_work_t tw[cpus];
    pthread_t thread[cpus];

    for (int i=0; i < cpus; i++) {
        tw[i].start_n = (nNodes/cpus)* i    + 1;
        tw[i].end_n   = (nNodes/cpus)*(i+1);
    
        pthread_create(&thread[i], NULL, warshall, (void*)&tw[i]);
    }

    struct thread_work_t maintw;

    maintw.start_n = (nNodes/cpus) * cpus + 1;
    maintw.end_n   = nNodes;
    warshall((void*)&maintw);

    for (int i=0; i<cpus; i++) {
        pthread_join(thread[i], NULL);
    }
  write(stdout);

  free(graph);

  return 0;
}

